/**
 * This gruntfile is here to support development of 
 * this theme. In production mode, sass and js files
 * are imported in the project-specific theme, and
 * this gruntfile is not needed at all then!
 */

module.exports = function(grunt) {
    'use strict';

    var path = require( "path" );

    grunt.initConfig({
        pkg: {
            name: 'GRUNTICON'
        },

        watch: {
            css: {
                files: ['sass/**/*.scss'],
                tasks: ['sass', 'autoprefixer'],
                options: {
                    livereload: true,
                },
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'images/source/raw',
                    src: ['*.svg'],
                    dest: 'images/source/optimized'
                }]
            }
        },        

        grunticon: {
            icons: {
                files: [{
                    expand: true,
                    cwd: 'images/source/optimized',
                    src: ['**/*.svg', '**/*.png'],
                    dest: "images/dest"
                }],
                options: {

                    // CSS filenames
                    datasvgcss: "icons.data.svg.css",
                    datapngcss: "icons.data.png.css",
                    urlpngcss: "icons.fallback.css",

                    // preview HTML filename
                    previewhtml: "preview.html",

                    // grunticon loader code snippet filename
                    loadersnippet: "grunticon.loader.js",

                    // Include loader code for SVG markup embedding
                    enhanceSVG: true,

                    // Make markup embedding work across domains (if CSS hosted externally)
                    corsEmbed: false,

                    // folder name (within dest) for png output
                    pngfolder: "png",

                    // prefix for CSS classnames
                    cssprefix: ".icon-",

                    defaultWidth: "300px",
                    defaultHeight: "200px",

                    // define vars that can be used in filenames if desirable, like foo.colors-primary-secondary.svg
                    colors: {
                        primary: "red",
                        secondary: "#666"
                    },

                    dynamicColorOnly: true,

                    // css file path prefix - this defaults to "/" and will be placed before the "dest" path when stylesheets are loaded.
                    // This allows root-relative referencing of the CSS. If you don't want a prefix path, set to to ""
                    cssbasepath: "/css",
                    customselectors: {
                        "cat" : ["#el-gato"],
                        "gummy-bears-2" : ["nav li a.deadly-bears:before"]
                    },

                    template: path.join( __dirname, "images", "default-css.hbs" ),
                    previewTemplate: path.join( __dirname, "images", "preview-custom.hbs" ),

                    compressPNG: true

                }
            }
        },        

        sass: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'css/styles.css': ['sass/styles.scss']
                }
            }
        },


        autoprefixer: {
            sassprefix: {
                options: {
                    map: true
                    //browsers: ['last 2 versions', 'ie 9']
                },

                src: 'css/styles.css',
                dest: 'css/styles.css'
            }
        },




    });






    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-autoprefixer');

    grunt.loadNpmTasks( 'grunt-svgmin' );
    grunt.loadNpmTasks( 'grunt-grunticon' );

    grunt.registerTask('default', ['watch']);
    grunt.registerTask('icons', ['grunticon:icons']);

    grunt.registerTask('build', ['sass', 'concat', 'uglify', 'autoprefixer']);
};
